%http://localhost:8090/api/test/clienteJson/10/circulo
%http://localhost:8090/api/test/clienteJson/10/cuadrado
-module(test).

-export([start/0, clienteJson/3]).

-import(string, [concat/2]).

start() ->
    inets:start(httpd,
                [{modules,
                  [mod_alias,
                   mod_auth,
                   mod_esi,
                   mod_actions,
                   mod_cgi,
                   mod_dir,
                   mod_get,
                   mod_head,
                   mod_log,
                   mod_disk_log]},
                 {port, 8090},
                 {server_name, "shell"},
                 {server_root, "/tmp"},
                 {document_root, "/usr/lib/erlang/lib/inets-7.1.2/examples/server_root/htdocs"},
                 {erl_script_alias, {"/api", [test]}},
                 {error_log, "error.log"},
                 {security_log, "security.log"},
                 {transfer_log, "transfer.log"},
                 {mime_types,
                  [{"html", "text/html"},
                   {"css", "text/css"},
                   {"js", "application/x-javascript"},
                   {"json", "application/json"}]}]).

clienteJson(SessionID, _Env, _Input) ->
    [Num, Tipo] = string:split(_Input, "/"),
    NumF = list_to_integer(Num),
    A = generateList(NumF),
    Par = even(A),
    Sum = sum(Par),
    Area = tipoFigura(Tipo, Sum),
    Arre = io_lib:format("~p", [Par]),
    Sup = io_lib:format("~p", [Area]),
    
    Str1 = "\"El arreglo de pares es:\"",
    Str2 = "\"EL Area es:\"",
    mod_esi:deliver(SessionID,
                    ["Content-Type: application/json\r\n\r\n", [Str1, Arre], [Str2, Sup]]).

%Genear números aleatorios
generateList(Num) ->
    Arreglo = [rand:uniform(Num) || I <- lists:seq(1, Num)],
    Arreglo.


%Verificar el tipo de figura
tipoFigura(Tipo, Sum) -> 
    T1 = string:equal(Tipo, "cuadrado"),
    T2 = string:equal(Tipo, "circulo"),
    R = if T1 ->
        is_area_Cuadrado(Sum);
    T2 ->
        is_area_Circulo(Sum)
    end,
    R.

%Extraer los pares

even(L) -> (even(L, [])).
even([], Acc) ->
    Acc;
even([H | T], Acc) when H rem 2 == 0 ->
    even(T, [H | Acc]);
even([_ | T], Acc) ->
    even(T, Acc).

%obtener la suma
sum(L) ->
    sum(L, 0).

sum([], Acc) ->
    Acc;
sum([N | Rest], Acc) ->
    sum(Rest, N + Acc).

%Area cuadrado
is_area_Cuadrado(Area_C) ->
    A = Area_C * Area_C,
    A.

%Area circulo
is_area_Circulo(Medida) ->
    M = 3.14 * (Medida * Medida),
    M.
